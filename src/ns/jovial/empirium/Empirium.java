package ns.jovial.empirium;

import java.io.IOException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ns.jovial.empirium.handlers.BanHandler;

import org.bukkit.Server;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import org.mcstats.Metrics;

import ns.jovial.empirium.handlers.CommandHandler;
import ns.jovial.empirium.listeners.PlayerListener;
import ns.jovial.empirium.utils.Data;

public class Empirium extends JavaPlugin {
    public static Logger log;
    public Server        server = this.getServer();

    // No need for these variables to be static.
    // We simply add this class to the constructor of the classes that use them.
    public String               pluginVersion;
    public String               pluginName;
    public List<String>         pluginAuthors;
    public Empirium             kPl;
    public ConfigurationSection playerFile;
    public CommandHandler       commandhandler;
    public Data                 data;
    public FileConfiguration    config;
    public BanHandler           banhandler;

    private void init() {

        // metrics @ http://mcstats.org/plugin/Kvarstaadt
        try {
            Metrics metrics = new Metrics(this);

            metrics.start();
        } catch(IOException ex) {
            log.log(Level.WARNING, "Failed to submit metrics data: {0}", ex.getMessage());
        }

        // Config files.
        saveDefaultConfig();

        // Plugin Manager
        final PluginManager pm = kPl.getServer().getPluginManager();

        pm.registerEvents(new PlayerListener(kPl), kPl);

        // Command Handler
        commandhandler.registerCommands();
    }

    public void load() {
        banhandler     = new BanHandler();
        data           = new Data(this);
        commandhandler = new CommandHandler(this);
        log            = server.getLogger();
        pluginName     = this.getDescription().getName();
        pluginVersion  = this.getDescription().getVersion();
        pluginAuthors  = this.getDescription().getAuthors();
        kPl            = this;
        playerFile     = config.getConfigurationSection("players");
        config         = getConfig();
    }

    @Override
    public void onDisable() {
        kPl = null;
        log.log(Level.INFO, "{0} has been disabled.", pluginName);
    }

    @Override
    public void onEnable() {
        init();
        load();
        log.log(Level.INFO, "{0} v{1} by {2}", new Object[] { pluginName, pluginVersion, pluginAuthors });

        // Ban waves
        BukkitScheduler scheduler = getServer().getScheduler();

        scheduler.scheduleSyncRepeatingTask(this,
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    banhandler.executeWaveBan();
                                                }
                                            },
                                            0L,
                                            1728000L);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
