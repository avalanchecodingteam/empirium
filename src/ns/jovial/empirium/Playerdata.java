package ns.jovial.empirium;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;

public class Playerdata {
    private static Map<String, Playerdata> playerList          = new HashMap<>();
    private static Empirium                plugin              = new Empirium();
    private static ConfigurationSection    playerFile          = plugin.playerFile;
    public static String                   DATE_STORAGE_FORMAT = "EEE, d MMM yyyy HH:mm:ss Z";
    private static String                  name;
    private String                         rank;
    private String                         custom_login_message;
    private String                         custom_join_message;
    private String                         custom_leave_message;

    public Playerdata(String name, ConfigurationSection section) {
        Playerdata.name           = name.toLowerCase();
        this.rank                 = section.getString("custom_login", "");
        this.custom_login_message = section.getString("custom_login", "");
        this.custom_join_message  = section.getString("custom_join", "");
        this.custom_leave_message = section.getString("custom_leave", "");
    }

    public Playerdata(String name, String rank, String custom_login_message, String custom_join_message,
                      String custom_leave_message) {
        Playerdata.name           = name.toLowerCase();
        this.rank                 = rank.toLowerCase();
        this.custom_login_message = custom_login_message;
        this.custom_join_message  = custom_join_message;
        this.custom_leave_message = custom_leave_message;
    }

    public static void add(String pname) {
        try {
            pname = pname.toLowerCase();

            if(playerList.containsKey(pname)) {

                // do nothing
            } else {
                String     rank                 = "Guest";
                String     custom_login_message = "";
                String     custom_join_message  = "";
                String     custom_leave_message = "";
                Playerdata player               = new Playerdata(pname,
                                                                 rank,
                                                                 custom_login_message,
                                                                 custom_join_message,
                                                                 custom_leave_message);

                playerList.put(pname, player);
            }
        } catch(Exception ex) {
            plugin.log.severe(Arrays.toString(ex.getStackTrace()));
        }
    }

    public static String dateToString(Date date) {
        return new SimpleDateFormat(DATE_STORAGE_FORMAT, Locale.ENGLISH).format(date);
    }

    public static Date stringToDate(String date_str) {
        try {
            return new SimpleDateFormat(DATE_STORAGE_FORMAT, Locale.ENGLISH).parse(date_str);
        } catch(ParseException ex) {
            return new Date(0L);
        }
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();

        try {
            output.append("Name: ").append(Playerdata.name).append("\n");
            output.append("- Rank: ").append(this.rank).append("\n");
            output.append("- Custom Login Message: ").append(this.custom_login_message).append("\n");
            output.append("- Custom Join Message: ").append(this.custom_join_message).append("\n");
            output.append("- Custom Leave Message: ").append(this.custom_leave_message).append("\n");
        } catch(Exception ex) {
            plugin.log.severe(Arrays.toString(ex.getStackTrace()));
        }

        return output.toString();
    }

    public String setCustomJoin(String custom_join) {
        this.custom_join_message = custom_join;

        return custom_join;
    }

    public String getCustomJoinMessage() {
        return custom_join_message;
    }

    public String setCustomLeave(String custom_leave) {
        this.custom_leave_message = custom_leave;

        return custom_leave;
    }

    public String getCustomLeaveMessage() {
        return custom_leave_message;
    }

    public String setCustomLogin(String custom_login) {
        this.custom_login_message = custom_login;

        return custom_login;
    }

    public String getCustomLoginMessage() {
        return custom_login_message;
    }

    public static Playerdata getEntry(String name) {
        Playerdata.name = name.toLowerCase();

        if(playerList.containsKey(name)) {
            return playerList.get(name);
        } else {
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public String getRank() {
        return rank;
    }

    public String setRank(String rank) {
        this.rank = rank;

        return rank;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
