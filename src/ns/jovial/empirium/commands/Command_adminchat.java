package ns.jovial.empirium.commands;

import org.apache.commons.lang.StringUtils;

import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.utils.ChatUtils;
import ns.jovial.empirium.utils.Util;

//@CommandOptions(usage="/<command> [message]", description="Adminchat.", permission="nno.admin", source=SourceType.ANY)
public class Command_adminchat implements CommandExecutor {
    private final Empirium plugin;

    public Command_adminchat(Empirium aThis) {
        plugin = aThis;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        if(!(sender instanceof Player)) {
            if(args.length == 0) {
                sender.sendMessage("Only in-game players can toggle admin chat.");

                return true;
            } else {
                Util.adminChatMessage(sender, ChatUtils.colorize(StringUtils.join(args, " ")));
            }
        }

        Player p = (Player) sender;

        if(args.length == 0) {
            Util userinfo = Util.getPlayerInfo(p);

            userinfo.setAdminChat(!userinfo.inAdminChat());
            p.sendMessage(ChatColor.GRAY + "Toggled Admin Chat " + (userinfo.inAdminChat()
                                                                    ? "on"
                                                                    : "off") + ".");
        } else {
            Util.adminChatMessage(sender, ChatUtils.colorize(StringUtils.join(args, " ")));
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
