package ns.jovial.empirium.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.utils.ChatUtils;

//@CommandOptions(usage = "/<command> [on|off]", description="You cannot die", permission = "jovial.buddha", source = SourceType.PLAYER)
public class Command_buddha implements CommandExecutor {
    private final Empirium plugin;

    public Command_buddha(Empirium plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        if(((args.length == 1) && "off".equalsIgnoreCase(args[0]))
                || ((args.length == 1) && "on".equalsIgnoreCase(args[0]))) {
            if("on".equalsIgnoreCase(args[0])) {
                if(!plugin.data.buddhaMode.contains((Player) sender)) {
                    plugin.data.buddhaMode.add((Player) sender);
                    ((Player) sender).setHealth(1);
                } else {

                    // Do nothing
                }

                sender.sendMessage(ChatUtils.colorize(plugin.config.getString("buddha_mode_on")
                                                                   .replace("%player%", sender.getName())));

                return true;
            }

            if("off".equalsIgnoreCase(args[0])) {
                if(plugin.data.buddhaMode.contains((Player) sender)) {
                    plugin.data.buddhaMode.remove((Player) sender);
                    ((Player) sender).setHealth(((Player) sender).getMaxHealth());
                } else {

                    // Do nothing
                }

                sender.sendMessage(ChatUtils.colorize(plugin.config.getString("buddha_mode_off")
                                                                   .replace("%player%", sender.getName())));

                return true;
            }
        } else {
            if(!plugin.data.buddhaMode.contains((Player) sender)) {
                plugin.data.buddhaMode.add((Player) sender);
                sender.sendMessage(ChatUtils.colorize(plugin.config.getString("buddha_mode_on")
                                                                   .replace("%player%", sender.getName())));
                ((Player) sender).setHealth(1);

                return true;
            } else {
                plugin.data.buddhaMode.remove((Player) sender);
                sender.sendMessage(ChatUtils.colorize(plugin.config.getString("buddha_mode_off")
                                                                   .replace("%player%", sender.getName())));
                ((Player) sender).setHealth(((Player) sender).getMaxHealth());

                return true;
            }
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
