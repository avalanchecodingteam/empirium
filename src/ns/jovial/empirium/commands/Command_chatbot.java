package ns.jovial.empirium.commands;

import org.apache.commons.lang.StringUtils;

import org.bukkit.ChatColor;
import org.bukkit.command.*;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.utils.ChatUtils;

//@CommandOptions(description="NNO Chatbot", usage="/chatbot <message>", aliases="cbot", permission="nno.chatbot", source=SourceType.ANY)
public class Command_chatbot implements CommandExecutor {
    private final Empirium plugin;

    public Command_chatbot(Empirium plugin) {
        this.plugin = plugin;
    }

    public void ChatBot(String message) {
        plugin.log.info(message);
        ChatUtils.messagePlayers(message, "nno.admin");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please specify a message!");

            return false;
        } else {
            ChatBot(ChatUtils.colorize(StringUtils.join(args, " ")));
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
