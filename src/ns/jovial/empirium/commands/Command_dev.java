package ns.jovial.empirium.commands;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.listeners.ServerListener;
import ns.jovial.empirium.utils.CommandUtils;
import ns.jovial.empirium.utils.PluginUtils;

//@CommandOptions(usage="/<command> <reload | recalc | eval | debug>", description="Developer command.", permission="nno.developer", source=SourceType.PLAYER)
public class Command_dev implements CommandExecutor {
    private static Empirium plugin = new Empirium();
    private final boolean   recalc = false;

    public Command_dev(Empirium plugin) {
        Command_dev.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        if(args.length == 2) {
            switch(args[0]) {
                case "reload" :
                    CommandUtils.info(sender, "Reloading plugin...");

                    try {
                        PluginUtils.reloadPlugin(plugin.kPl);
                        CommandUtils.info(sender, "Plugin reloaded.");
                    } catch(Exception ex) {
                        CommandUtils.severe(sender, "Failed to reload!");
                    }

                    break;

                case "recalc" :
                    try {
                        Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
                        Player                       p       = (Player) players;

                        p.recalculatePermissions();
                    } catch(Exception ex) {
                        CommandUtils.severe(sender, ex.getMessage());
                    } finally {
                        CommandUtils.warning(sender, "Player permissions " + (recalc
                                                                              ? "successfully "
                                                                              : "unsuccessfully ") + "recalculated.");
                    }

                    break;

                case "eval" :
                    Player pl = (Player) sender;

                    CommandUtils.info(sender, "Sending server evaluation.");
                    CommandUtils.info(sender, getInfo(pl));

                    break;

                case "debug" :
                    CommandUtils.warning(sender, "This command is currently unimplemented!");

                    break;
            }
        } else {
            return false;
        }

        return true;
    }

    private String getInfo(Player p) {
        StringBuilder sb = new StringBuilder();

        try {
            sb.append("Server Info: \n");
            sb.append("- Server Name: ").append(plugin.server.getServerName()).append("\n");
            sb.append("- Bukkit Version: ").append(plugin.server.getBukkitVersion()).append("\n");
            sb.append("- Default Gamemode: ").append(plugin.server.getDefaultGameMode().toString()).append("\n");
            sb.append("- Idle Timeout: ").append(plugin.server.getIdleTimeout()).append("\n");
            sb.append("- Server IP: ").append(plugin.server.getIp()).append("\n");
            sb.append("- Server Port: ").append(plugin.server.getPort()).append("\n");
            sb.append("- Current Plugin Count: ")
              .append(plugin.server.getPluginManager().getPlugins().length)
              .append("\n");
            sb.append("- Server Ping: ").append(ServerListener.getPing(p));
        } catch(Exception ex) {
            plugin.log.severe(ex.getMessage());
            CommandUtils.severe(p, ex.getMessage());
        }

        return sb.toString();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
