package ns.jovial.empirium.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import ns.jovial.empirium.Empirium;

//@CommandOptions(usage = "/<command>", description="Plugin info", permission = "jovial.kvsi", source = SourceType.ANY)
public class Command_kvsi implements CommandExecutor {
    private final Empirium plugin;

    public Command_kvsi(Empirium plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        sender.sendMessage(ChatColor.YELLOW + plugin.pluginName + " v" + plugin.pluginVersion + " by "
                           + plugin.pluginAuthors + " is currently enabled on this server.");

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
