package ns.jovial.empirium.commands;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.Playerdata;

//@CommandOptions(usage="/<command> <create | join | leave | login | rank <rank>> [message]", description = "Change your custom login, join, and leave messages.", permission="jovial.personal", source=SourceType.PLAYER)
public class Command_personal implements CommandExecutor {
    private final List<String> ACCESS = Arrays.asList("smack17", "paldiu");
    private final Empirium     plugin;

    public Command_personal(Empirium plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        Player     p    = (Player) sender;
        String     name = p.getName().toLowerCase();
        Playerdata pd   = Playerdata.getEntry(name);

        switch(args.length) {
            case 1 :
                if(args[0].equalsIgnoreCase("create")) {
                    if(!plugin.playerFile.isString(name)) {
                        Playerdata.add(name);
                        p.sendMessage("Your name has been put in the file.");
                    } else {
                        p.sendMessage("You're already in the file!");
                    }

                    return true;
                }

                break;

            case 2 :
                switch(args[0]) {
                    case "join" :
                        pd.setCustomJoin(StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " "));

                        return true;

                    case "login" :
                        pd.setCustomLogin(StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " "));

                        return true;

                    case "leave" :
                        pd.setCustomLeave(StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " "));

                        return true;
                }
            case 3 :
                if(args[0].equalsIgnoreCase("rank")) {
                    if(!ACCESS.contains(sender.getName().toLowerCase())) {
                        sender.sendMessage(ChatColor.YELLOW + "You don't have permission!");

                        return true;
                    } else {
                        Player     t  = Bukkit.getServer().getPlayer(args[1]);
                        Playerdata pl = Playerdata.getEntry(t.getName());

                        pl.setRank(args[2]);
                    }
                }

                break;

            default :
                break;
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
