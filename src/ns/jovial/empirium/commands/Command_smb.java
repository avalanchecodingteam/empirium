package ns.jovial.empirium.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;

//@CommandOptions(usage="/<command> <mobType>", description = "An universal mob command. With added features.", permission="jovial.smb", source=SourceType.PLAYER)
public class Command_smb implements CommandExecutor {
    public Command_smb(Empirium plugin) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        EntityType[] et = EntityType.values();

        switch(args.length) {
            case 0 :
                return false;

            case 1 :
                if(Arrays.toString(et).matches(args[0])) {
                    Player p = (Player) sender;

                    p.getWorld().spawnEntity(p.getEyeLocation(), EntityType.fromName(args[0]));
                } else if(args[0].equalsIgnoreCase("list")) {
                    sender.sendMessage(ChatColor.YELLOW + "Available mob types: " + "\n"
                                       + Arrays.toString(Arrays.toString(et).split(", ")));
                } else {
                    sender.sendMessage(ChatColor.RED + "Invalid mob! Use /smb list to list all mob types.");
                }

                return true;

            case 2 :
                return false;
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
