package ns.jovial.empirium.commands;

import org.apache.commons.lang3.StringUtils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static org.bukkit.Bukkit.getPlayer;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.utils.ChatUtils;

//@CommandOptions(usage = "/<command> <<tag>|off|clear>", description="Set your tag.", permission = "jovial.tag", source = SourceType.PLAYER)
public class Command_tag implements CommandExecutor {
    Empirium plugin;

    public Command_tag(Empirium plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        Player playerSender = (Player) sender;

        if("clear".equalsIgnoreCase(args[0])) {
            if(args.length != 1) {
                return false;
            }

            int count = 0;

            for (Player p : plugin.data.playerTags.keySet()) {
                String origtag = p.getDisplayName();

                p.setDisplayName(origtag.replace(plugin.data.playerTags.get(p) + " ", ""));
                count++;
            }

            plugin.data.playerTags.clear();
            ChatUtils.bCastMsg(ChatUtils.colorize(plugin.config.getString("messages.clear_tags")));
            sender.sendMessage(ChatColor.GRAY + "Cleared the tags of " + String.valueOf(count) + " players.");

            return true;
        } else if("off".equalsIgnoreCase(args[0])) {
            if(args.length == 2) {
                if(!sender.hasPermission("jovial.tag.others")) {
                    return false;
                }

                Player plr = getPlayer(args[1]);

                if(plr == null) {
                    sender.sendMessage(ChatColor.GRAY + "Player not found!");

                    return true;
                }

                String origtag = plr.getDisplayName();

                plr.setDisplayName(origtag.replace(plugin.data.playerTags.get(plr) + " ", ""));
                plugin.data.playerTags.remove(plr);
                sender.sendMessage(ChatColor.GRAY + "Removed tag for " + plr.getName() + ".");

                return true;
            } else if(args.length != 1) {
                return false;
            }

            String origtag = playerSender.getDisplayName();

            playerSender.setDisplayName(origtag.replace(plugin.data.playerTags.get(playerSender) + " ", ""));
            plugin.data.playerTags.remove(playerSender);
            sender.sendMessage(ChatColor.GRAY + "Removed your tag.");
        } else {
            if(args.length < 1) {
                return false;
            }

            if(StringUtils.join(args, " ").length() > plugin.config.getInt("tag_limit")) {
                sender.sendMessage(ChatColor.GRAY + "Your tag exceeded the tag limit at "
                                   + String.valueOf(plugin.config.getInt("tag_limit")) + ".");

                return true;
            }

            if(plugin.data.playerTags.containsKey(playerSender)) {
                String newtag  = ChatUtils.colorize(StringUtils.join(args, " "));
                String origtag = playerSender.getDisplayName();

                playerSender.setDisplayName(newtag + " "
                                            + origtag.replace(plugin.data.playerTags.get(playerSender) + " ",
                                                              ""));
                plugin.data.playerTags.put(playerSender, newtag);
                sender.sendMessage(ChatColor.GRAY + "Set your tag to " + newtag + ChatColor.GRAY + ".");

                return true;
            }
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
