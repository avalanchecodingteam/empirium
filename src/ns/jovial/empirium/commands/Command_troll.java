package ns.jovial.empirium.commands;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.utils.WorldUtils;

//@CommandOptions(usage="/<command> <rsheep | rbl | rde> <player>", description="Troll command. For fun only.", permission="nno.troll", source=SourceType.ANY)
public class Command_troll implements CommandExecutor {
    public Command_troll(Empirium plugin) {}

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        Player p = (Player) sender;

        if(args.length != 2) {
            return false;
        }

        Random random = new Random();
        Player t      = Bukkit.getServer().getPlayer(args[1]);

        switch(args[0]) {
            case "rsheep" :
                EntityType e = EntityType.SHEEP;

                t.getWorld().spawnEntity(t.getEyeLocation(), e);

                return true;

            case "rbl" :
                Material fm = Material.values()[random.nextInt(Material.values().length)];

                t.getWorld().getBlockAt(p.getEyeLocation()).setType(fm);

                return true;

            case "rde" :
                EntityType ent = EntityType.values()[random.nextInt(EntityType.values().length)];

                t.getWorld().spawnEntity(t.getEyeLocation(), ent);

                return true;

            case "rtp" :
                double   d1 = random.nextDouble() + random.nextInt(1000);
                double   d2 = random.nextDouble() + random.nextInt(1000);
                double   d3 = random.nextDouble() + random.nextInt(1000);
                Location l  = new Location(t.getWorld(), d1, d2, d3);

                t.teleport(WorldUtils.getSafeLocation(l));
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
