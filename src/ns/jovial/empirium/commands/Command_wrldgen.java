package ns.jovial.empirium.commands;

import org.bukkit.WorldCreator;
import org.bukkit.command.*;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.utils.CommandUtils;
import ns.jovial.empirium.worldgen.*;

//@CommandOptions(usage="/<command> <gen <worldName> | create <structure type> | slist>", description="Generate new worlds, and create structures.", source=SourceType.ANY)
public class Command_wrldgen implements CommandExecutor {
    private final String            names = "house, pyramid, jumgletemple, deserttemple, seatemple, rigatoni";
    private CleanroomBlockPopulator cbp;
    private CleanroomChunkGenerator ccg;

    public Command_wrldgen(Empirium plugin) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
        if(args.length == 1) {
            switch(args[0]) {
                case "slist" :
                    CommandUtils.info(sender, "Structure List: " + names);

                    break;
            }
        }

        if(args.length == 2) {
            switch(args[0]) {
                case "create" :
                    switch(args[1]) {
                        case "house" :
                            break;

                        case "pyramid" :
                            break;

                        case "jungletemple" :
                            break;

                        case "deserttemple" :
                            break;

                        case "seatemple" :
                            break;

                        case "rigatoni" :
                            break;
                    }

                    break;

                case "gen" :
                    WorldCreator wc = new WorldCreator(args[1]);

                    wc.generator(new CleanroomChunkGenerator());

                    break;
            }
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
