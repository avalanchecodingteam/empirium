package ns.jovial.empirium.handlers;

import java.util.List;

import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;

public class BanHandler {
    public Empirium plugin = new Empirium();

    public void addIpBan(Player player) {
        List<String> bans = plugin.config.getStringList("banned_ips");

        bans.add(player.getAddress().getHostName());
        plugin.config.set("banned_ips", bans);
        plugin.saveConfig();
    }

    public void addIpBan(String playerip) {
        List<String> bans = plugin.config.getStringList("banned_ips");

        bans.add(playerip);
        plugin.config.set("banned_ips", bans);
        plugin.saveConfig();
    }

    public void addNameBan(Player player) {
        List<String> bans = plugin.config.getStringList("banned_names");

        bans.add(player.getName());
        plugin.config.set("banned_names", bans);
        plugin.saveConfig();
    }

    public void addNameBan(String playerName) {
        List<String> bans = plugin.config.getStringList("banned_names");

        bans.add(playerName);
        plugin.config.set("banned_names", bans);
        plugin.saveConfig();
    }

    public void addPlayerBan(Player player) {
        addIpBan(player);
        addNameBan(player);
    }

    public void addWaveBan(Player player) {
        List<String> bans = plugin.config.getStringList("banwave.names");

        bans.add(player.getName());
        plugin.config.set("banwave.names", bans);

        List<String> ipbans = plugin.config.getStringList("banwave.ips");

        bans.add(player.getAddress().getHostName());
        plugin.config.set("banwave.ips", bans);
        plugin.saveConfig();
    }

    public void addWaveBan(String playername, String playerip) {
        List<String> bans = plugin.config.getStringList("banwave.names");

        bans.add(playername);
        plugin.config.set("banwave.names", bans);

        List<String> ipbans = plugin.config.getStringList("banwave.ips");

        bans.add(playerip);
        plugin.config.set("banwave.ips", bans);
        plugin.saveConfig();
    }

    public void executeWaveBan() {
        List<String> bans = plugin.config.getStringList("banwave.names");

        for (String name : plugin.config.getStringList("banwaves.names")) {
            bans.add(name);
        }

        plugin.config.set("banned_names", bans);

        List<String> ipbans = plugin.config.getStringList("banwave.ips");

        for (String name : plugin.config.getStringList("banwaves.ips")) {
            ipbans.add(name);
        }

        plugin.config.set("ips", ipbans);
        plugin.saveConfig();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
