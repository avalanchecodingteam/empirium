package ns.jovial.empirium.handlers;

import org.bukkit.command.CommandExecutor;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.commands.Command_adminchat;
import ns.jovial.empirium.commands.Command_cclr;
import ns.jovial.empirium.commands.Command_chatbot;
import ns.jovial.empirium.commands.Command_dev;
import ns.jovial.empirium.commands.Command_kvsi;
import ns.jovial.empirium.commands.Command_personal;
import ns.jovial.empirium.commands.Command_smb;
import ns.jovial.empirium.commands.Command_troll;
import ns.jovial.empirium.commands.Command_wrldgen;

public class CommandHandler {
    private final Empirium plugin;

    public CommandHandler(Empirium aThis) {
        plugin = aThis;
    }

    public void registerCommand(String command, CommandExecutor executor) {
        plugin.getCommand(command).setExecutor(executor);
    }

    public void registerCommands() {
        registerCommand("adminchat", new Command_adminchat(plugin));
        registerCommand("cclr", new Command_cclr(plugin));
        registerCommand("chatbot", new Command_chatbot(plugin));
        registerCommand("dev", new Command_dev(plugin));
        registerCommand("kvsi", new Command_kvsi(plugin));
        registerCommand("personal", new Command_personal(plugin));
        registerCommand("smb", new Command_smb(plugin));
        registerCommand("troll", new Command_troll(plugin));
        registerCommand("wrldgen", new Command_wrldgen(plugin));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
