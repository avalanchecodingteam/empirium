package ns.jovial.empirium.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import ns.jovial.empirium.Empirium;

public class EntityListener implements Listener {
    private static Empirium plugin;

    public EntityListener(Empirium plugin) {
        EntityListener.plugin = plugin;
    }

    @EventHandler
    public void entityDamageEvent(org.bukkit.event.entity.EntityDamageEvent e) {
        Entity entity = e.getEntity();

        if(entity instanceof Player) {
            Player player = (Player) entity;

            if(plugin.data.buddhaMode.contains(player)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void entityRegainHealthEvent(org.bukkit.event.entity.EntityRegainHealthEvent e) {
        Entity entity = e.getEntity();

        if(entity instanceof Player) {
            Player player = (Player) entity;

            if(plugin.data.buddhaMode.contains(player)) {
                e.setCancelled(true);
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
