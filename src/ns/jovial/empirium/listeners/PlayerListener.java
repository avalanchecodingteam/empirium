package ns.jovial.empirium.listeners;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import ns.jovial.empirium.Empirium;
import ns.jovial.empirium.utils.ChatUtils;
import ns.jovial.empirium.utils.Util;

public class PlayerListener implements Listener {
    private static Empirium plugin;

    public PlayerListener(Empirium plugin) {
        PlayerListener.plugin = plugin;
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        try {
            final Player player  = e.getPlayer();
            String       message = e.getMessage().trim();
            Util         util    = Util.getPlayerInfo(player);

            if(util.inAdminChat()) {
                Util.adminChatMessage(player, message);
                e.setCancelled(true);
            }
        } catch(Exception ex) {
            plugin.log.severe(Arrays.toString(ex.getStackTrace()));
        }
    }

    @EventHandler
    public void onPlayerConnect(AsyncPlayerPreLoginEvent e) {
        if(plugin.config.getStringList("banned_names").contains(e.getName())) {
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED,
                       ChatUtils.colorize(plugin.config.getString("ban_message")));
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        if(plugin.playerFile.contains(p.getName())) {
            if(!plugin.playerFile.getString(p.getName() + ".custom_join").isEmpty()) {
                for (Player plr : plugin.server.getOnlinePlayers()) {
                    plr.sendMessage((String[]) plugin.playerFile.getStringList(p.getName() + ".custom_join").toArray());
                }
            } else {

                // do nothing
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        if(plugin.playerFile.contains(p.getName())) {
            if(!plugin.playerFile.getString(p.getName() + ".custom_leave").isEmpty()) {
                e.setQuitMessage(plugin.playerFile.getString(p.getName() + ".custom_leave"));
            } else {

                // do nothing
            }
        }
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();

        if(plugin.playerFile.contains(p.getName())) {
            if(!plugin.playerFile.getString(p.getName() + ".custom_login").isEmpty()) {
                for (Player op : Bukkit.getServer().getOnlinePlayers()) {
                    op.sendMessage(ChatUtils.colorize(ChatColor.AQUA + p.getName() + " is "
                                                      + plugin.playerFile.getString(p.getName() + ".custom_login")));
                }
            } else {

                // do nothing
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
