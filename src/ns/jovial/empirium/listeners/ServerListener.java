
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package ns.jovial.empirium.listeners;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.SheepRegrowWoolEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.inventory.ItemStack;

import ns.jovial.empirium.Empirium;

public class ServerListener implements Listener {
    private static final Random  random = new Random();
    private static Empirium      plugin;
    private final List<DyeColor> COLOR_POOL = Arrays.asList(DyeColor.BLACK,
                                                            DyeColor.BLUE,
                                                            DyeColor.BROWN,
                                                            DyeColor.CYAN,
                                                            DyeColor.GRAY,
                                                            DyeColor.GREEN,
                                                            DyeColor.LIGHT_BLUE,
                                                            DyeColor.LIME,
                                                            DyeColor.MAGENTA,
                                                            DyeColor.ORANGE,
                                                            DyeColor.PINK,
                                                            DyeColor.PURPLE,
                                                            DyeColor.RED,
                                                            DyeColor.SILVER,
                                                            DyeColor.WHITE,
                                                            DyeColor.YELLOW);
    private final int x = 0;

    public ServerListener(Empirium plugin) {
        ServerListener.plugin = plugin;
    }

    @EventHandler
    public void onLeafDecay(LeavesDecayEvent e) {
        Block    b = e.getBlock();
        Location l = b.getLocation();
        World    w = b.getWorld();
        int      y = random.nextInt(100) + 1;

        if(y <= 14) {
            List<Material> m = Arrays.asList(Material.BOAT_ACACIA,
                                             Material.BAKED_POTATO,
                                             Material.ANVIL,
                                             Material.CLAY_BRICK,
                                             Material.PAINTING,
                                             Material.SKULL_ITEM);
            ItemStack is = new ItemStack(m.get(random.nextInt(m.size())), 1);

            w.dropItemNaturally(l, is);
        }
    }

    @EventHandler
    public void onLightningStrikeEvent(LightningStrikeEvent e) {
        Location l = e.getLightning().getLocation();
        World    w = e.getLightning().getWorld();

        if(w.getBlockAt(l).isLiquid()) {
            List<LivingEntity> ent    = w.getLivingEntities();
            LivingEntity       entity = ent.get(ent.size());
            World              wld    = entity.getWorld();
            Location           loc    = entity.getLocation();
            Block              b      = wld.getBlockAt(loc);

            if(b.isLiquid() && w.getNearbyEntities(l, 50, 50, 50).contains(entity)) {
                entity.setHealth(0.0);
            }
        }
    }

    @EventHandler
    public void onSheepRegrowWoolEvent(SheepRegrowWoolEvent e) {
        Sheep sheep = e.getEntity();

        sheep.setColor(COLOR_POOL.get(random.nextInt(COLOR_POOL.size())));
    }

    @EventHandler
    public void playerInCombat(PlayerEvent e) {
        Player p = e.getPlayer();
    }

    public static int getPing(Player p) {
        Class<?> clazz;
        String   bpName  = Bukkit.getServer().getClass().getPackage().getName(),
                 version = bpName.substring(bpName.lastIndexOf(".") + 1, bpName.length());

        try {
            clazz = Class.forName("org.bukkit.craftbukkit." + version + ".entity.CraftPlayer");

            Object CraftPlayer  = clazz.cast(p);
            Method getHandle    = CraftPlayer.getClass().getMethod("getHandle", new Class[0]);
            Object EntityPlayer = getHandle.invoke(CraftPlayer, new Object[0]);
            Field  ping         = EntityPlayer.getClass().getDeclaredField("ping");

            return ping.getInt(EntityPlayer);
        } catch(ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException | NoSuchFieldException ex) {
            plugin.log.severe(ex.getMessage());
        }

        return 0;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
