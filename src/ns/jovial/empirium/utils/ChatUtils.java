package ns.jovial.empirium.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatUtils {
    public static void bCastMsg(String string) {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            p.sendMessage(string);
        }
    }

    public static String colorize(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    public static void messagePlayers(String string, String permission) {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            if(p.hasPermission(permission)) {
                p.sendMessage(string);
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
