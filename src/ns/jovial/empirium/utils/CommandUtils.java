package ns.jovial.empirium.utils;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author AvalancheDT
 */
public class CommandUtils {

    /**
     * Sends the user an error message, saying their command was invalid.
     *
     * <p>
     * This method automatically sends a pre-defined message prefix and color.</p>
     * @param sender The user who sent the command
     * @param msg The message in question
     */
    public static void error(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.WHITE + "ERROR" + ChatColor.DARK_GRAY + "] "
                           + ChatColor.GRAY + msg);
    }

    /**
     * Sends the user a message.
     *
     * <p>
     * This method automatically sends a pre-defined message prefix and color.</p>
     * @param sender The user who sent the command
     * @param msg The message in question
     */
    public static void info(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.BLUE + "INFO" + ChatColor.DARK_GRAY + "] "
                           + ChatColor.GRAY + msg);
    }

    /**
     * Sends a message stating the sender doesn't have permission to use the command.
     *
     * <p>
     * This method automatically sends a pre-defined message prefix and color.</p>
     * @param sender The sender who dispatched the command.
     */
    public static void noPerms(CommandSender sender) {
        sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "Kvarstaadt" + ChatColor.DARK_GRAY + "] "
                           + ChatColor.YELLOW + "You don't have permission to use this command!");
    }

    /**
     * Sends the player a message.
     *
     * <p>
     * A variation of playerMsg using CommandSender instead of Player.</p>
     * @param sender The person who sent the command.
     * @param msg
     */
    public static void playerMsg(CommandSender sender, String msg) {
        Player p = (Player) sender;

        playerMsg(p, msg);
    }

    /**
     * Sends the player a message.
     *
     * <p>
     * A cleaner version of sender.sendMessage();</p>
     * @param p The person who sent the command.
     * @param msg
     */
    public static void playerMsg(Player p, String msg) {
        p.sendMessage(msg);
    }

    /**
     * Sends a severe warning message to the sender about their command input.
     *
     * <p>
     * This method automatically sends a pre-defined message prefix and color. </p>
     * @param sender The original command sender, such as the player who sent the command.
     * @param msg The message to be displayed.
     */
    public static void severe(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "SEVERE" + ChatColor.DARK_GRAY + "] "
                           + ChatColor.GRAY + msg);
    }

    /**
     * Sends a message stating that the command is unknown.
     *
     * @param sender The sender who dispatched the command.
     */
    public static void unknown(CommandSender sender) {
        sender.sendMessage(ChatColor.WHITE + "Unknown Command.");
    }

    /**
     * Sends the user a warning message, saying their command threw an error.
     *
     * <p>
     * This method automatically sends a pre-defined message prefix and color.</p>
     * @param sender The user who sent the command
     * @param msg The message in question
     */
    public static void warning(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.YELLOW + "WARNING" + ChatColor.DARK_GRAY + "] "
                           + ChatColor.GRAY + msg);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
