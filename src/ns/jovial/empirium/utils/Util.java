package ns.jovial.empirium.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import ns.jovial.empirium.Empirium;

public class Util {
    private static final Empirium             plugin       = new Empirium();
    public final static Map<Player, Util>     userinfo     = new HashMap<>();
    private static final List<String>         ranks        = Arrays.asList("owner",
                                                                           "co-owner",
                                                                           "developer",
                                                                           "admin",
                                                                           "mod");
    private static final ConfigurationSection players      = plugin.playerFile;
    private boolean                           in_adminchat = false;
    private final Player                      player;
    private final String                      ip_address;
    private final String                      player_name;

    public Util(Player player) {
        this.player      = player;
        this.ip_address  = player.getAddress().getAddress().getHostAddress();
        this.player_name = player.getName();
    }

    public static void adminChatMessage(CommandSender sender, String message) {
        String name = ChatColor.DARK_RED + sender.getName() + " " + getPrefix(sender);

        plugin.log.log(Level.INFO, "[ADMIN] {0}: {1}", new Object[] { name, message });
        ChatUtils.messagePlayers(ChatColor.WHITE + "[" + ChatColor.AQUA + "ADMIN" + ChatColor.WHITE + "] " + name
                                 + ChatColor.WHITE + ": " + ChatColor.GOLD + message,
                                 "nno.admin");
    }

    public boolean inAdminChat() {
        return in_adminchat;
    }

    public void setAdminChat(boolean in_adminchat) {
        this.in_adminchat = in_adminchat;
    }

    public static Util getPlayerInfo(Player player) {
        Util ut = Util.userinfo.get(player);

        if(ut == null) {
            Iterator<Map.Entry<Player, Util>> it = userinfo.entrySet().iterator();

            while(it.hasNext()) {
                Map.Entry<Player, Util> pair    = it.next();
                Util                    ut_test = pair.getValue();

                if(ut_test.player_name.equalsIgnoreCase(player.getName())) {
                    if(Bukkit.getOnlineMode()) {
                        ut = ut_test;

                        break;
                    } else {
                        if(ut_test.ip_address.equalsIgnoreCase(player.getAddress().getAddress().getHostAddress())) {
                            ut = ut_test;

                            break;
                        }
                    }
                }
            }
        }

        if(ut == null) {
            ut = new Util(player);
            Util.userinfo.put(player, ut);
        }

        return ut;
    }

    public static String getPrefix(CommandSender sender) {
        String prefix = null;

        if(sender instanceof Player) {
            if(players.contains(sender.getName().toLowerCase())
                    && ranks.contains(players.getString(sender.getName().toLowerCase() + ".rank"))) {
                switch(players.getString(sender.getName().toLowerCase() + ".rank")) {
                    case "owner" :
                        prefix = ChatColor.DARK_GRAY + "[" + ChatColor.BLUE + "Owner" + ChatColor.DARK_GRAY + "]";

                        break;

                    case "co-owner" :
                        prefix = ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "Co-Owner" + ChatColor.DARK_GRAY + "]";

                        break;

                    case "admin" :
                        prefix = ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "Op" + ChatColor.DARK_GRAY + "]";

                        break;

                    case "mod" :
                        prefix = ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "Mod" + ChatColor.DARK_GRAY + "]";

                        break;

                    case "developer" :
                        prefix = ChatColor.DARK_GRAY + "[" + ChatColor.LIGHT_PURPLE + "Dev" + ChatColor.DARK_GRAY + "]";

                        break;
                }
            }
        } else {
            prefix = ChatColor.DARK_GRAY + "[" + ChatColor.DARK_PURPLE + "Console" + ChatColor.DARK_GRAY + "]";
        }

        return prefix + ChatColor.WHITE;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
