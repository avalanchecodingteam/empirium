package ns.jovial.empirium.utils;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;

import static org.bukkit.Material.AIR;

import ns.jovial.empirium.Empirium;

public class WorldUtils {
    private static final Empirium plugin = new Empirium();

    public static Location getRandomLocation(World world) {
        Random rand = new Random();
        int    x    = rand.nextInt(10000);
        int    z    = rand.nextInt(10000);

        return new Location(world, x, 60, z);
    }

    public static Location getSafeLocation(Location loc) {
        int      safeY = 255;
        int      safeX = loc.getBlockX();
        int      safeZ = loc.getBlockZ();
        Location l     = loc;

        l.setY(safeY);

        while(l.getBlock().getType() == AIR) {
            safeY--;
            l.setY(safeY);
        }

        Location ll = l;

        ll.setY(l.getY() + 1);

        while(plugin.config.getList("blocked").contains(l.getBlock().getTypeId())
                || plugin.config.getList("blocked").contains(ll.getBlock().getTypeId())) {
            l.setX(safeX + 1);
            safeX++;
        }

        l.setY(safeY + 1);

        return l;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
